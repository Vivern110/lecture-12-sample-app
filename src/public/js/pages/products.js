require.config({
    baseUrl: './js',
    paths: {
        jquery: './libs/jquery'
    }
});

require(['jquery', './Cart'], function ($, Cart) {
    function checkCartButtons(button, productId) {
        if (!button.data('available')) {
            button
                .text('Unavailable')
                .addClass('unavailable')
                .prop('disabled', 'disabled');
        } else {
            if (Cart.has(productId)) {
                button
                    .text('Already Added')
                    .removeClass('add')
                    .addClass('remove');
            } else {
                button
                    .text('Add to Cart')
                    .removeClass('remove')
                    .addClass('add');
            }
        }
    }

    function checkCartCounter() {
        $('.cart-strip .count').text(Cart.count() || 'Empty');
    }

    $(function () {
        $('.product').each(function () {
            var product = $(this), productId = product.data('id');
            product.find('button.action').each(function () {
                var button = $(this).click(function () {
                    Cart[Cart.has(productId) ? 'remove' : 'add'].call(Cart, productId);
                    checkCartButtons(button, productId);
                    checkCartCounter();
                });

                checkCartButtons(button, productId);
                checkCartCounter();
            });
        });

        $('.cart-strip a').click(function (e) {
            e.preventDefault();
            window.location.href = './cart?order=' + Cart.serialize();
        });
    })
});