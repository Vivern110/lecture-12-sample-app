require.config({
    baseUrl: './js',
    paths: {
        jquery: './libs/jquery'
    }
});

require(['jquery', './Cart'], function ($, Cart) {
    $(function () {
        $('.cart .actions').each(function () {
            var actions = $(this);
            actions.find('[data-action=recalculate]').click(function () {
                $('.cart .product').each(function () {
                    var product = $(this), productId = product.data('id');
                    Cart.add(productId, parseInt(product.find('input').val()) || 0);
                    window.location.replace('./cart?order=' + Cart.serialize());
                });
            });

            actions.find('[data-action=order]').click(function () {
                var email = window.prompt('Provide us with your email, please!');
                if (email) {
                    var phone = window.prompt('Provide us with your phone number, please!');
                    if (phone) {
                        $.post('./cart?order=' + Cart.serialize() + '&email=' + email + '&phone=' + phone)
                            .done(function (data) {
                                Cart.removeAll();
                                window.location.replace('./order/' + data.orderId);
                            })
                            .fail(function () {
                                alert('Something went wrong with you request. Please try again later!');
                            });
                    }
                }
            });
        });
    })
});